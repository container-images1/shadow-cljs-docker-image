FROM clojure:openjdk-11-lein-slim-buster

RUN apt --yes update
RUN apt --yes install npm
RUN npm install shadow-cljs