# shadow-cljs-docker-image

Pre-built docker image with shadow-cljs deps and multiarch support, build in the usual way:
```
git clone https://gitlab.com/container-images1/shadow-cljs-docker-image.git
cd shadow-cljs-docker-image
docker build -t shadow-cljs:testing .
```
